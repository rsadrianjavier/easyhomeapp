﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Gestor.Web.Models
{
    public class AdminCatalogoList
    {
        public int InmuebleId { get; set; }

        public string InmuebleName { get; set; }

        public double Price { get; set; }

        public string Direction { get; set; }

        public string Planta { get; set; }

        public string Publicado { get; set; }

    }
}