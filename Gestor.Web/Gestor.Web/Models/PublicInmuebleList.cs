﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Gestor.Web.Models
{
    public class PublicInmuebleList
    {
        public int InmuebleId { get; set; }

        public string InmuebleName { get; set; }

        public double Price { get; set; }

        public string Direction { get; set; }

        public string Planta { get; set; }

        public int Metros { get; set; }

        public int Habitaciones { get; set; }

        public bool Piscina { get; set; }

        public bool Ascensor { get; set; }

        public string Image { get; set; }

        public string Description { get; set; }

    }
}