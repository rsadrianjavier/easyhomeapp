﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Gestor.DAL;
using Gestor.CORE.Contracts;
using Gestor.Application;

namespace Gestor.Web.Client
{
    public partial class MisInmueblesList : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        IInmuebleManager inmuebleManager = null;

        int id;

        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            inmuebleManager = new InmuebleManager(context);
        }

        protected void btnEliminar_Click(int id)
        {
            try
            {

                var inmueble = inmuebleManager.GetById(id);
                if (inmueble != null)
                {
                    inmuebleManager.Remove(inmueble);

                    context.SaveChanges();

                    Response.Redirect("~/Client/MisInmueblesList.aspx");
                }

            }
            catch (Exception ex)
            {
       
                var err = new CustomValidator
                {
                    ErrorMessage = "Se ha producido un error al eliminar el inmueble",
                    IsValid = false
                };
                Page.Validators.Add(err);
            }
        }
    }
}