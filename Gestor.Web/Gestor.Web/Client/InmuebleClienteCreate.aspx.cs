﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Gestor.DAL;
using Gestor.CORE.Contracts;
using Gestor.Application;
using Image = Gestor.CORE.Photo;
using Gestor.CORE;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Gestor.Web.Client
{
    public partial class InmuebleClienteCreate : System.Web.UI.Page
    {
        InmuebleManager inmuebleManager = null;
        ImageManager imageManager = null;
        DirectionManager directionManager = null;
        UserManager<ApplicationUser> userManager;
        ApplicationUser currentUser;
        protected void Page_Load(object sender, EventArgs e)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            inmuebleManager = new InmuebleManager(context);
            imageManager = new ImageManager(context);
            directionManager = new DirectionManager(context);
            userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            currentUser = userManager.FindByIdAsync(User.Identity.GetUserId()).Result;
        }


        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                Image imagenInmueble = new Image()
                {
                    Name = txtNombre.Text,
                    ImageBytes = System.Convert.ToBase64String(FileUpload.FileBytes)
                };
                imageManager.Add(imagenInmueble);
                imageManager.Context.SaveChanges();
                IQueryable<Image> imagenGuardada = imageManager.GetImageByName(txtNombre.Text);

                Direction direccionInmueble = new Direction()
                {
                    Location = txtCiudad.Text,
                    Province = txtProvincia.Text,
                    Street = txtCalle.Text,
                    Street2 = txtCalle2.Text
                };
                directionManager.Add(direccionInmueble);

                Inmueble inmueble = new Inmueble
                {
                    InmuebleName = txtNombre.Text,
                    Price = double.Parse(txtPrecio.Text),
                    metros = int.Parse(TextBoxMetros.Text),

                    Captado = true,
                    Publicado = false,

                    DirectionId = direccionInmueble.DirectionId,
                    Direction = direccionInmueble,

                    Habitaciones = int.Parse(txtHabitaciones.Text),
                    Planta = txtPlanta.Text,

                    Piscina = checkPiscina.Checked,
                    Ascensor = checkAscensor.Checked,
                    exterior = checkExterior.Checked,

                    Description = txtDescripcion.Text,

                    Image = imagenGuardada.First(),
                    ImageId = imagenGuardada.First().ImageId,

                    ClientId = currentUser.Id
                };
                inmuebleManager.Add(inmueble);
                inmuebleManager.Context.SaveChanges();
                Response.Redirect("~/Client/CreateConfirm.aspx", false);
            }
            catch (Exception ex)
            {
                var err = new CustomValidator
                {
                    ErrorMessage = "Se ha producido un error al guardar",
                    IsValid = false
                };
            }

        }

    }
}