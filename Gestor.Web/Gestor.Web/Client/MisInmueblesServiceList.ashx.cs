﻿using Gestor.Application;
using Gestor.CORE;
using Gestor.DAL;
using Gestor.Web.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Script.Serialization;

namespace Gestor.Web.Client
{
    /// <summary>
    /// Servicio que obtiene los inmuebles del cliente
    /// </summary>
    public class MisInmueblesServiceList : System.Web.UI.Page, IHttpHandler
    {

        override public void ProcessRequest(HttpContext context)
        {
            var iDisplayLength = int.Parse(context.Request["length"]);
            var iDisplayStart = int.Parse(context.Request["start"]);
            var sSearch = context.Request["search[value]"];
            var iSortDir = context.Request["order[0][dir]"];
            var iSortCol = context.Request["order[0][column]"];
            var sSortColum = context.Request["columns[" + iSortCol + "][data]"];

            //var sSortColum = context.Request.QueryString["mDataProp_" + iSortCol];

            ApplicationDbContext contextdb = new ApplicationDbContext();
            InmuebleManager inmuebleManager = new InmuebleManager(contextdb);
            UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(contextdb));
            ApplicationUser currentUser = userManager.FindByIdAsync(User.Identity.GetUserId()).Result;

            #region select
            var allInmuebles = inmuebleManager.GetInmueblesCliente(currentUser.Id);
            var inmuebles = allInmuebles
                .Select(p => new ClientMisInmueblesList
                {
                    InmuebleId = p.InmuebleId,
                    InmuebleName = p.InmuebleName,
                    Price = p.Price,
                    Direction = p.Direction.Street + " - " + p.Direction.Location + " (" + p.Direction.Province + ")",
                    Planta = p.Planta
                });
            #endregion

            #region Filter
            if (!string.IsNullOrWhiteSpace(sSearch))
            {
                string where = @"InmuebleId.ToString().Contains(@0) ||
                                InmuebleName.ToString().Contains(@0) ||
                                Price.ToString().Contains(@0) ||
                                Direction.ToString().Contains(@0) ||
                                Planta.ToString().Contains(@0)";
                inmuebles = inmuebles.Where(where, sSearch);
            }
            #endregion

            #region Paginate
            if (!string.IsNullOrWhiteSpace(sSortColum))
            {
                inmuebles = inmuebles
                        .OrderBy(sSortColum + " " + iSortDir)
                        .Skip(iDisplayStart)
                        .Take(iDisplayLength);
            }
            #endregion
            var result = new
            {
                iTotalRecords = allInmuebles.Count(),
                iTotalDisplayRecords = allInmuebles.Count(),
                aaData = inmuebles
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(result);
            context.Response.ContentType = "application/json";
            context.Response.Write(json);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}