﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Gestor.Web._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" />

    <div class="jumbotron">
        <h1>Trabajo Fin de Master</h1>
        <p class="lead">GESTIÓN Y DESARROLLO DE APLICACIONES MULTIPLATAFORMA</p>
        <p>
            <a href="/About.aspx" class='btn btn-success btn-lg'><span class='glyphicon glyphicon-book'></span>&nbsp  Ir a la documentación &raquo;</a>
        </p>
    </div>

    <div class="jumbotron">

        <a class="card-text text-center alert alert-success" href="/Public/Catalogue.aspx">
            <asp:Image style="vertical-align:text-top" ImageUrl="~/favicon.ico" Height="23px" runat="server" />
            &nbsp Ir a la aplicación &raquo;
        </a>
        <br />
        <br />
        <p>Para comenzar a utilizar las funcionalidades de la aplicación como administrador, debe logarse con las siguientes credenciales: "admin@admin.es" con la contraseña "admin"</p>
        <p>Una vez hecho esto podrá gestionar el rol del resto de usuarios, para dar acceso a los agentes a la parte privada como administradores en la sección "Gestión de usuarios".</p>
        <p>Para acceder a la aplicación como cliente, simplemente debe registrarse y acceder con sus credenciales.</p>
    </div>
</asp:Content>
