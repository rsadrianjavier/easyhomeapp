﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="Gestor.Web.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2 class='card-text text-center alert alert-success'>Documentación TFM</h2>
    <hr />

    <iframe src="documentacion.pdf" width="100%" height="680px"></iframe>

</asp:Content>
