﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="Gestor.Web.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2 class='card-text text-center alert alert-success'>Contacto</h2>
    <hr />
    <address>
        <p class="bold"><strong>SEAS Estudios Superiores Abiertos</strong></p>
        Violeta Parra, 9 50015 Zaragoza<br />
        <abbr title="Teléfono">Tel:</abbr>
        976 700 660
    </address>

    <address>
        <a href="https://www.campusseas.com">www.campusseas.com</a><br />
    </address>
</asp:Content>
