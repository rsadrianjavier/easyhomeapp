﻿<%@ Page Title="Detalle Inmueble" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="InmuebleView.aspx.cs" Inherits="Gestor.Web.Public.InmuebleView" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h2 class='card-text text-center alert alert-success'><%: Title %></h2>

    <div style="padding-top: 50px">
        <div class="col-md-3">
                <asp:Image ID="imgInmueble" runat="server" height='180px' class='card-img-top Image_<%: id %>' src="" />
        </div>
        <div class="col-md-9">
            <div class="form-group">
                <asp:Label ID="LabelNombre" runat="server" Text="Nombre" CssClass="col-md-3" AssociatedControlID="txtNombre"></asp:Label>
                <asp:Label ID="txtNombre" Text="" CssClass="col-md9" runat="server" />
            </div>
            <div class="form-group">
                <asp:Label ID="LabelDescripcion" runat="server" Text="Descripción" CssClass="col-md-3" AssociatedControlID="txtDescripcion"></asp:Label>
                <asp:Label ID="txtDescripcion" Text="" CssClass="col-md9" runat="server" />
            </div>
            <div class="form-group">
                <asp:Label ID="LabelPrecio" runat="server" Text="Precio" CssClass="col-md-3" AssociatedControlID="txtPrecio"></asp:Label>
                <asp:Label ID="txtPrecio" Text="" CssClass="col-md9" runat="server" />
            </div>
        </div>
    </div>

    <br /><br />
</asp:Content>
