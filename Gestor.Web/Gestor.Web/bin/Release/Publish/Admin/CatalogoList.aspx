﻿<%@ Page Title="Gestión Catálogo" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CatalogoList.aspx.cs" Inherits="Gestor.Web.Admin.CatalogoList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
    <h2 class='card-text text-center alert alert-success'>Gestión de inmuebles del catálogo</h2>
    <br />
    <table id="inmueblesTable" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Precio</th>
                <th>Dirección</th>
                <th>Planta</th>
                <th></th>
            </tr>
        </thead>
    </table>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
        $(document).ready( function () {
            $('#inmueblesTable').DataTable({
                "processing": true, // for show progress bar
                "serverSide": true, // for process server side
                "filter": true, // this is for disable filter (search box)
                "orderMulti": false, // for disable multiple column at once
                "ajax": {
                    "url": '/Admin/CatalogoServiceList.ashx',
                    "type": "POST",
                    "datatype": "json"
                },
                "language": {
                    'url': "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                },
                'columns': [
                    { "data": "InmuebleId", "Name": "InmuebleId", "autoWidth": true },
                    { "data": "InmuebleName", "Name": "InmuebleName", "autoWidth": true },
                    { "data": "Price", "Name": "Price", "autoWidth": true },
                    { "data": "Direction", "Name": "Direction", "autoWidth": true },
                    { "data": "Planta", "Name": "Planta", "autoWidth": true },
                                        {
                        "data": null,
                        "className": "button",
                        "orderable": "false"
                    }
                ],
                'columnDefs': [
                    {
                        "render": function (data, type, row) {
                            return "<a href='/Admin/InmuebleEdit?id=" + row.InmuebleId + "'>" + data + "</a>";
                        },
                        "targets": 1,
                        "render": function (data, type, row) {
                            return "<a data-id='" + row.InmuebleId + "' class='btn btn-danger delete-inmuebleCatalogo' title='Eliminar inmueble'><span class='glyphicon glyphicon-trash'></span></a>";
                        },
                        "targets": 5
                    },
                    {

                        "orderable": false,
                        "targets": 5
                    }
                ]
            });
        } );
    </script>

</asp:Content>
