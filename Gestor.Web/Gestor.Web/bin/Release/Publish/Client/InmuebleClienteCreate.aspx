﻿<%@ Page Title="Agregar Inmueble" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="InmuebleClienteCreate.aspx.cs" Inherits="Gestor.Web.Client.InmuebleClienteCreate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="form-horizontal">
        <h2 class='card-text text-center alert alert-success'><%: Title %></h2>
        <hr />
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" />
        <div class="form-group">
            <asp:Label ID="LabelNombre" runat="server" Text="Nombre" CssClass="col-md-3" AssociatedControlID="txtNombre"></asp:Label>
            <div class="col-md9">
                <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Introduzca un nombre, por favor" ControlToValidate="txtNombre" Text="*"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <asp:Label ID="LabelPrecio" runat="server" Text="Precio" CssClass="col-md-3" AssociatedControlID="txtPrecio"></asp:Label>
            <div class="col-md9">
                <asp:TextBox ID="txtPrecio" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
        <div class="form-group">
            <asp:Label ID="LabelMetros" runat="server" Text="Metros cuadrados" CssClass="col-md-3" AssociatedControlID="TextBoxMetros"></asp:Label>
            <div class="col-md9">
                <asp:TextBox ID="TextBoxMetros" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>

        <div class="form-group">
            <asp:Label ID="Label4" runat="server" Text="Calle" CssClass="col-md-3" AssociatedControlID="txtCalle"></asp:Label>
            <div class="col-md9">
                <asp:TextBox ID="txtCalle" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
        <div class="form-group">
            <asp:Label ID="Label5" runat="server" Text="Nº" CssClass="col-md-3" AssociatedControlID="txtCalle2"></asp:Label>
            <div class="col-md9">
                <asp:TextBox ID="txtCalle2" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
        <div class="form-group">
            <asp:Label ID="Label6" runat="server" Text="Ciudad" CssClass="col-md-3" AssociatedControlID="txtCiudad"></asp:Label>
            <div class="col-md9">
                <asp:TextBox ID="txtCiudad" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
        <div class="form-group">
            <asp:Label ID="Label7" runat="server" Text="Provincia" CssClass="col-md-3" AssociatedControlID="txtProvincia"></asp:Label>
            <div class="col-md9">
                <asp:TextBox ID="txtProvincia" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>


        <div class="form-group">
            <asp:Label ID="Label1" runat="server" Text="Piscina" CssClass="col-md-3" AssociatedControlID="checkPiscina"></asp:Label>
            <div class="col-md9">
                <asp:CheckBox ID="checkPiscina" Text="" runat="server" />
            </div>
        </div>
        <div class="form-group">
            <asp:Label ID="Label2" runat="server" Text="Ascensor" CssClass="col-md-3" AssociatedControlID="checkAscensor"></asp:Label>
            <div class="col-md9">
                <asp:CheckBox ID="checkAscensor" Text="" runat="server" />
            </div>
        </div>
        <div class="form-group">
            <asp:Label ID="Label3" runat="server" Text="Exterior" CssClass="col-md-3" AssociatedControlID="checkExterior"></asp:Label>
            <div class="col-md9">
                <asp:CheckBox ID="checkExterior" Text="" runat="server" />
            </div>
        </div>

        <div class="form-group">
            <asp:Label ID="Label8" runat="server" Text="Nº de habitaciones" CssClass="col-md-3" AssociatedControlID="txtHabitaciones"></asp:Label>
            <div class="col-md9">
                <asp:TextBox ID="txtHabitaciones" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
        <div class="form-group">
            <asp:Label ID="Label9" runat="server" Text="Planta" CssClass="col-md-3" AssociatedControlID="txtPlanta"></asp:Label>
            <div class="col-md9">
                <asp:TextBox ID="txtPlanta" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>

        <div class="form-group">
            <asp:Label ID="LabelImagen" runat="server" Text="Imagen" CssClass="col-md-3" AssociatedControlID="FileUpload"></asp:Label>
            <asp:FileUpload ID="FileUpload" runat="server" />
        </div>
        <div class="form-group">
            <asp:Label ID="LabelDescripcion" runat="server" Text="Descripción" CssClass="col-md-3" AssociatedControlID="txtDescripcion"></asp:Label>
            <div class="col-md9">
                <asp:TextBox ID="txtDescripcion" runat="server" class="form-control" TextMode="SingleLine" Rows="2"></asp:TextBox>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md1 col-md-offset-3">
                <asp:Button ID="btnSubmit" runat="server" Text="Crear" class="btn btn-success" OnClick="btnSubmit_Click" />

            </div>
        </div>
    </div>
</asp:Content>

