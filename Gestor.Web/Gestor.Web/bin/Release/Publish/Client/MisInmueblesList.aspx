﻿<%@ Page Title="Mis inmuebles" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MisInmueblesList.aspx.cs" Inherits="Gestor.Web.Client.MisInmueblesList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
    <h2 class='card-text text-center alert alert-success'>Mis inmuebles</h2>
    <br />
    <table id="inmueblesTable" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Precio</th>
                <th>Dirección</th>
                <th>Planta</th>
                <th><asp:Button Text="Añadir inmueble" PostBackUrl="~/Client/InmuebleClienteCreate.aspx" class="btn btn-success" runat="server" /></th>
            </tr>
        </thead>
    </table>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
        $(document).ready( function () {
            $('#inmueblesTable').DataTable({
                "processing": true, // for show progress bar
                "serverSide": true, // for process server side
                "filter": true, // this is for disable filter (search box)
                "orderMulti": false, // for disable multiple column at once
                "ajax": {
                    "url": '/Client/MisInmueblesServiceList.ashx',
                    "type": "POST",
                    "datatype": "json"
                },
                "language": {
                    'url': "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                },
                'columns': [
                    { "data": "InmuebleId", "Name": "InmuebleId", "autoWidth": true },
                    { "data": "InmuebleName", "Name": "InmuebleName", "autoWidth": true },
                    { "data": "Price", "Name": "Price", "autoWidth": true },
                    { "data": "Direction", "Name": "Direction", "autoWidth": true },
                    { "data": "Planta", "Name": "Planta", "autoWidth": true },
                    {
                        "data": null,
                        "className": "button",
                        "orderable": "false"
                    }
                ],
                'columnDefs': [
                    {

                        "orderable": false,
                        "targets": 5
                    }
                ]
            });
        } );
    </script>

</asp:Content>
