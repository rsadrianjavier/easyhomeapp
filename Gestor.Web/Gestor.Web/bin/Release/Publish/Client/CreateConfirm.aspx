﻿<%@ Page Title="Confirmación" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CreateConfirm.aspx.cs" Inherits="Gestor.Web.Client.CreateConfirm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
        <h1>¡ Inmueble añadido con éxito !</h1>
        <p>Una vez haya verificado la información uno de nuestros agentes, se porcederá de inmediato a la publicación de su inmueble.</p>
        <br />
        <p class="lead">Para hacer cualquier consulta puedes ponerte en contacto con nosotros: <a href="/Contact.aspx" class=' alert-success'>Contacto</a></p>
    </div>

    <div class="jumbotron">
        <a class="btn btn-success btn-lg" href="/Client/MisInmueblesList.aspx">Volver a "Mis inmuebles" &raquo;</a>
    </div>
</asp:Content>
