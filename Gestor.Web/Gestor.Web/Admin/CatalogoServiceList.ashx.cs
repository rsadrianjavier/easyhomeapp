﻿using Gestor.Application;
using Gestor.CORE;
using Gestor.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Providers.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Web.UI;
using System.Web.Script.Serialization;
using System.Linq.Dynamic;
using Gestor.Web.Models;

namespace Gestor.Web.Admin
{
    /// <summary>
    /// Servicio que obtiene los inmuebles con estado publicado
    /// </summary>
    public class CatalogoServiceList : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var iDisplayLength = int.Parse(context.Request["length"]);
            var iDisplayStart = int.Parse(context.Request["start"]);
            var sSearch = context.Request["search[value]"];
            var iSortDir = context.Request["order[0][dir]"];
            var iSortCol = context.Request["order[0][column]"];
            var sSortColum = context.Request["columns[" + iSortCol + "][data]"];

            //var sSortColum = context.Request.QueryString["mDataProp_" + iSortCol];

            ApplicationDbContext contextdb = new ApplicationDbContext();
            InmuebleManager inmuebleManager = new InmuebleManager(contextdb);
            #region select
            var allInmuebles = inmuebleManager.GetInmueblesCaptados();
            var inmuebles = allInmuebles
                .Select(p => new AdminCatalogoList
                {
                    InmuebleId = p.InmuebleId,
                    InmuebleName = p.InmuebleName,
                    Price = p.Price,
                    Direction = p.Direction.Street + " - " + p.Direction.Location + " (" + p.Direction.Province + ")",
                    Planta = p.Planta,
                    Publicado = p.Publicado ? "Si" : "No"
                });
            #endregion

            #region Filter
            if (!string.IsNullOrWhiteSpace(sSearch))
            {
                string where = @"InmuebleId.ToString().Contains(@0) ||
                                InmuebleName.ToString().Contains(@0) ||
                                Price.ToString().Contains(@0) ||
                                Direction.ToString().Contains(@0) ||
                                Planta.ToString().Contains(@0) ||
                                Publicado.ToString().Contains(@0)";
                inmuebles = inmuebles.Where(where, sSearch);
            }
            #endregion

            #region Paginate
            if (!string.IsNullOrWhiteSpace(sSortColum))
            {
                inmuebles = inmuebles
                        .OrderBy(sSortColum + " " + iSortDir)
                        .Skip(iDisplayStart)
                        .Take(iDisplayLength);
            }
            #endregion
            var result = new
            {
                iTotalRecords = allInmuebles.Count(),
                iTotalDisplayRecords = allInmuebles.Count(),
                aaData = inmuebles
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(result);
            context.Response.ContentType = "application/json";
            context.Response.Write(json);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}