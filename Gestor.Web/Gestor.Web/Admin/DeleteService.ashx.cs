﻿using Gestor.Application;
using Gestor.CORE;
using Gestor.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Providers.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Web.UI;

namespace Gestor.Web.Admin
{
    /// <summary>
    /// Descripción breve de DeleteService
    /// </summary>
    public class DeleteService : System.Web.UI.Page, IHttpHandler
    {
        ApplicationDbContext contextdb = new ApplicationDbContext();

        override public void ProcessRequest(HttpContext context)
        {
            int inmuebletId = int.Parse(context.Request["inmuebleId"]);
            DeleteInmueble(inmuebletId);
        }


        private void DeleteInmueble(int inmuebleId)
        {


            InmuebleManager inmuebleManager = new InmuebleManager(contextdb);

            Inmueble inmueble = inmuebleManager.GetById(inmuebleId);
            inmuebleManager.Remove(inmueble);
            inmuebleManager.Context.SaveChanges();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}