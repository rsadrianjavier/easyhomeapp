﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Gestor.Application;
using Gestor.CORE;
using Gestor.CORE.Contracts;
using Gestor.DAL;

namespace Gestor.Web.Admin
{
    public partial class InmuebleEdit : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        IInmuebleManager inmuebleManager = null;

        int id;
        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            inmuebleManager = new InmuebleManager(context);

            if (!Page.IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    if (int.TryParse(Request.QueryString["id"], out id))
                    {
                        var inmueble = inmuebleManager.GetById(id);
                        if (inmueble != null)
                        {
                            LoadInmueble(inmueble);
                        }
                    }
                }
                else
                {
                    //TODO: Redirigir a listado
                }
            }
        }

        private void LoadInmueble(Inmueble inmueble)
        {
            TextBoxNombre.Text = string.IsNullOrEmpty(inmueble.InmuebleName) ? "" : inmueble.InmuebleName.ToString();
            TextBoxPrecio.Text = inmueble.Price.ToString();
            TextBoxDescripcion.Text = string.IsNullOrEmpty(inmueble.Description) ? "" : inmueble.Description.ToString();
            TextBoxMetros.Text = inmueble.metros.ToString();
            txtCalle.Text = string.IsNullOrEmpty(inmueble.Direction.Street) ? "" : inmueble.Direction.Street.ToString();
            txtCalle2.Text = string.IsNullOrEmpty(inmueble.Direction.Street2) ? "" : inmueble.Direction.Street2.ToString();
            txtCiudad.Text = string.IsNullOrEmpty(inmueble.Direction.Location) ? "" : inmueble.Direction.Location.ToString();
            txtProvincia.Text = string.IsNullOrEmpty(inmueble.Direction.Province) ? "" : inmueble.Direction.Province.ToString();
            checkAscensor.Checked = inmueble.Ascensor;
            checkExterior.Checked = inmueble.exterior;
            checkAscensor.Checked = inmueble.Ascensor;
            checkCaptado.Checked = inmueble.Captado;
            checkPublicado.Checked = inmueble.Publicado;

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["id"] != null)
                {
                    if (int.TryParse(Request.QueryString["id"], out id))
                    {
                        var inmueble = inmuebleManager.GetById(id);
                        if (inmueble != null)
                        {
                            inmueble.InmuebleName = TextBoxNombre.Text;
                            inmueble.Price = int.Parse(TextBoxPrecio.Text);
                            inmueble.Description = TextBoxDescripcion.Text;
                            inmueble.metros = int.Parse(TextBoxMetros.Text);
                            inmueble.InmuebleName = TextBoxNombre.Text;
                            inmueble.Direction.Street = txtCalle.Text;
                            inmueble.Direction.Street2 = txtCalle2.Text;
                            inmueble.Direction.Location = txtCiudad.Text;
                            inmueble.Direction.Province = txtProvincia.Text;
                            inmueble.Description = TextBoxDescripcion.Text;
                            inmueble.Ascensor = checkAscensor.Checked;
                            inmueble.exterior = checkExterior.Checked;
                            inmueble.Ascensor = checkAscensor.Checked;
                            inmueble.Captado = checkCaptado.Checked;
                            inmueble.Publicado = checkPublicado.Checked;

                            context.SaveChanges();

                            Response.Redirect("~/Admin/SeguimientoList.aspx");
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/Admin/SeguimientoList.aspx");
                }

            }
            catch (Exception ex)
            {
                var err = new CustomValidator
                {
                    ErrorMessage = "Se ha producido un error al modificar",
                    IsValid = false
                };
                Page.Validators.Add(err);
            }
        }
    }
}