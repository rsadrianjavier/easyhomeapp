﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Gestor.CORE.Contracts;
using Gestor.DAL;
using Gestor.Web.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Linq.Dynamic;

namespace Gestor.Web.Admin
{
    public partial class UserEdit : System.Web.UI.Page
    {
        ApplicationDbContext context = null;

        int id;
        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();

            var idUsuario = (Request.QueryString["id"]);

            var users = (from user in context.Users
                         select new
                         {
                             UserId = user.Id,
                             UserName = user.UserName,
                             Email = user.Email,
                             DNI = user.documentoIdentidad,
                             RoleNames = (from userRole in user.Roles
                                          join role in context.Roles on userRole.RoleId
                                          equals role.Id
                                          select role.Name).ToList()
                         }).ToList().Select(p => new AdminUserList()

                         {
                             UserId = p.UserId,
                             UserName = p.UserName,
                             Email = p.Email,
                             DNI = p.DNI,
                             Role = string.Join(",", p.RoleNames)
                         });



            if (!Page.IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {

                    string where = @"UserId.ToString().Contains(@0)";
                    users = users.Where(where, idUsuario);

                    var user = users.First();
                    if (user != null)
                    {
                        LoadUser(user);
                    }

                }
                else
                {
                    Response.Redirect("~/Admin/UserList.aspx");
                }
            }
        }

        private void LoadUser(AdminUserList user)
        {
            txtUserId.Text = user.UserId;
            txtUserName.Text = user.UserName;
            txtEmail.Text = user.Email;
            txtDNI.Text = user.DNI;
            if (user.Role.Equals("Client"))
            {
                txtRole.SelectedIndex = 1;
            }
            else
            {
                txtRole.SelectedIndex = 0;
            }

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            if (Request.QueryString["id"] != null)
            {
                var idUsuario = (Request.QueryString["id"]);

                var users = (from userEdited in context.Users
                             select new
                             {
                                 UserId = userEdited.Id,
                                 UserName = userEdited.UserName,
                                 Email = userEdited.Email,
                                 DNI = userEdited.documentoIdentidad,
                                 RoleNames = (from userRole in userEdited.Roles
                                              join role in context.Roles on userRole.RoleId
                                              equals role.Id
                                              select role.Name).ToList()
                             }).ToList().Select(p => new AdminUserList()

                             {
                                 UserId = p.UserId,
                                 UserName = p.UserName,
                                 Email = p.Email,
                                 DNI = p.DNI,
                                 Role = string.Join(",", p.RoleNames)
                             });

                string where = @"UserId.ToString().Contains(@0)";
                users = users.Where(where, idUsuario);

                var user = users.First();

                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();

                var entidad = context.Users.Where(w => w.Id == user.UserId).SingleOrDefault();
                var entidadRol = context.Roles.Where(w => w.Name == txtRole.SelectedItem.Value).SingleOrDefault();

                if (entidad != null)
                {
                    entidad.Roles.Clear();

                    entidad.Id = txtUserId.Text;
                    entidad.UserName = txtUserName.Text;
                    entidad.Email = txtEmail.Text;
                    entidad.documentoIdentidad = txtDNI.Text;

                    entidad.Roles.Add(new Microsoft.AspNet.Identity.EntityFramework.IdentityUserRole()
                    {
                        UserId = entidad.Id,
                        RoleId = entidadRol.Id
                    });                    

                    context.SaveChanges();
                }
            }

            Response.Redirect("~/Admin/UserList.aspx");


        }
    }
}