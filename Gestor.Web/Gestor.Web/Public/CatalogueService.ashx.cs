﻿
using Gestor.Application;
using Gestor.DAL;
using Gestor.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Script.Serialization;

namespace Gestor.Web.Public
{
    /// <summary>
    /// Servicio que obtiene los inmuebles del catálogo
    /// </summary>
    public class CatalogueService : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            ApplicationDbContext contextdb = new ApplicationDbContext();
            InmuebleManager inmuebleManager = new InmuebleManager(contextdb);

            #region select
            var allInmuebles = inmuebleManager.GetInmueblesPublicados();
            var inmuebles = allInmuebles
                .Select(p => new PublicInmuebleList
                {
                    InmuebleId = p.InmuebleId,
                    InmuebleName = p.InmuebleName,
                    Price = p.Price,
                    Description = p.Description,
                    Ascensor = p.Ascensor,
                    Habitaciones = p.Habitaciones,
                    Direction = p.Direction.Street + ", " + p.Direction.Street2 + ", " + p.Direction.Location + " (" + p.Direction.Province + ")",
                    Metros = p.metros,
                    Piscina = p.Ascensor,
                    Planta = p.Planta,
                    Image = p.Image.ImageBytes
                });
            #endregion

            IEnumerable<PublicInmuebleList> result = inmuebles;
            var serializer = new JavaScriptSerializer();
            context.Response.ContentType = "application/json";
            context.Response.Write(serializer.Serialize(result));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}