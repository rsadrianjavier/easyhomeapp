﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Gestor.Application;
using Gestor.CORE;
using Gestor.CORE.Contracts;
using Gestor.DAL;


namespace Gestor.Web.Public
{
    public partial class InmuebleView : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        IInmuebleManager inmuebleManager = null;
        IImageManager imageManager = null;
        public int id;

        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            inmuebleManager = new InmuebleManager(context);
            imageManager = new ImageManager(context);

            if (!Page.IsPostBack)
            {
                try
                {
                    if (Request.QueryString["id"] != null)
                    {
                        if (int.TryParse(Request.QueryString["id"], out id))
                        {
                            var inmueble = inmuebleManager.GetById(id);
                            inmueble.Image = new CORE.Photo();
                            inmueble.Image = imageManager.GetById(inmueble.ImageId);
                            if (inmueble != null)
                            {
                                LoadInmueble(inmueble);
                            }
                        }
                    }
                    else
                    {
                        //TODO: Redirigir a listado
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
        }

        private void LoadInmueble(Inmueble inmueble)
        {
            TextBoxNombre.Text = string.IsNullOrEmpty(inmueble.InmuebleName) ? " " : inmueble.InmuebleName.ToString();
            TextBoxPrecio.Text = string.IsNullOrEmpty(inmueble.Price.ToString()) ? " " : inmueble.Price.ToString();
            TextBoxDescripcion.Text = string.IsNullOrEmpty(inmueble.Description) ? " " : inmueble.Description.ToString();
            TextBoxMetros.Text = string.IsNullOrEmpty(inmueble.metros.ToString()) ? " " : inmueble.metros.ToString();
            txtCalle.Text = string.IsNullOrEmpty(inmueble.Direction.Street) ? " " : inmueble.Direction.Street.ToString();
            txtCalle2.Text = string.IsNullOrEmpty(inmueble.Direction.Street2) ? " " : inmueble.Direction.Street2.ToString();
            txtCiudad.Text = string.IsNullOrEmpty(inmueble.Direction.Location) ? " " : inmueble.Direction.Location.ToString();
            txtProvincia.Text = string.IsNullOrEmpty(inmueble.Direction.Province) ? " " : inmueble.Direction.Province.ToString();
            checkPiscina.Text = inmueble.Piscina ? "Si" : "No";
            checkExterior.Text = inmueble.exterior ? "Si" : "No";
            checkAscensor.Text = inmueble.Ascensor ? "Si" : "No";

            imgInmueble.Attributes.Add("src", "data:image/jpg;base64," + inmueble.Image.ImageBytes);
        }
    }
}
