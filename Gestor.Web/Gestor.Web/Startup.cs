﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Gestor.Web.Startup))]
namespace Gestor.Web
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
