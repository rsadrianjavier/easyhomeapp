﻿
$(document).on('click', ".delete-inmuebleCatalogo", function () {
    var inmuebleId = $(this).data("id");
    $.ajax({
        type: "POST",
        url: "/Admin/DeleteService.ashx",
        data: { inmuebleId: inmuebleId },
        success: function (data) {
            window.location.href = "CatalogoList.aspx";
        }
    });
});

$(document).on('click', ".delete-inmuebleSeguimiento", function () {
    var inmuebleId = $(this).data("id");
    $.ajax({
        type: "POST",
        url: "/Admin/DeleteService.ashx",
        data: { inmuebleId: inmuebleId },
        success: function (data) {
            window.location.href = "SeguimientoList.aspx";
        }
    });
});


