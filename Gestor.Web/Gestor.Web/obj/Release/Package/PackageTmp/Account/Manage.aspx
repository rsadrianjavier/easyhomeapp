﻿<%@ Page Title="Administrar cuenta" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Manage.aspx.cs" Inherits="Gestor.Web.Account.Manage" %>

<%@ Register Src="~/Account/OpenAuthProviders.ascx" TagPrefix="uc" TagName="OpenAuthProviders" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <h2 class='card-text text-center alert alert-success'><%: Title %></h2>

    <div>
        <asp:PlaceHolder runat="server" ID="successMessage" Visible="false" ViewStateMode="Disabled">
            <p class="text-success"><%: SuccessMessage %></p>
        </asp:PlaceHolder>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-horizontal">
                <h4>Editar datos de usuario</h4>
                <hr />
                <dl class="dl-horizontal">
                    <dt>Contraseña:</dt>
                    <dd>
                        <asp:HyperLink NavigateUrl="/Account/ManagePassword" Text="[Modificar]" Visible="false" ID="ChangePassword" runat="server" />
                        <asp:HyperLink NavigateUrl="/Account/ManagePassword" Text="[Crear]" Visible="false" ID="CreatePassword" runat="server" />
                    </dd>
                </dl>
<%--                <dl class="dl-horizontal">
                    <dt>
                        <asp:Label AssociatedControlID="txtNombre" Text="Nombre:" runat="server" /></dt>
                    <dd>
                        <asp:TextBox ID="txtNombre" runat="server" /></dd>
                </dl>--%>
                <dl class="dl-horizontal">
                    <dt>
                        <asp:Label AssociatedControlID="txtDNI" Text="D.N.I.:" runat="server" /></dt>
                    <dd>
                        <asp:TextBox ID="txtDNI" runat="server" /></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>
                        <asp:Label AssociatedControlID="txtDNI" Text="Teléfono:" runat="server" /></dt>
                    <dd>
                        <asp:TextBox ID="txtTelefono" runat="server" /></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>
                        <asp:Label AssociatedControlID="txtEmail" Text="Email:" runat="server" /></dt>
                    <dd>
                        <asp:TextBox ID="txtEmail" runat="server" /></dd>
                </dl>

            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-offset-2 col-md-10">
            <asp:Button runat="server" Text="Guardar cambios" ValidationGroup="" OnClick="SaveChanges" CssClass="btn btn-default" />
        </div>
    </div>

</asp:Content>
