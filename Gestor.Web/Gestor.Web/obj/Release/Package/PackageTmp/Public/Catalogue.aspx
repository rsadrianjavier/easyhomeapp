﻿<%@ Page Title="Catálogo" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Catalogue.aspx.cs" Inherits="Gestor.Web.Public.Catalogue" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row">
        <div id="listadoInmuebles" class="col-md-12">
            
        </div>
    </div>
    

    <script type="text/javascript">
        $(document).ready(function () {
    		let container = document.querySelector("#listadoInmuebles");
    	    container.innerHTML = "";
            var inmuebles = new Array();
            inmuebles = $.ajax({
                type: "POST",
                url: "CatalogueService.ashx",
                success: function (inmuebles) { 
                    var n = 0;
                    var sessionID = "<%= Session.SessionID %>";
                    $.each(inmuebles, function() {
                        var inmueble = document.createElement("div");
                        inmueble.setAttribute("class", "col-12 col-sm-6 col-md-4");
                        inmueble.innerHTML = 
                                                "<div class='card' style='width: 18rem;margin-top:50px'>" +
                                                    "<img id='ItemPreview_" + inmuebles[n].InmuebleId + "' src='' class='card-img-top Image_" + inmuebles[n].InmuebleId + "' height='180px' width='180px'  alt='Imagen del inmueble'>" +
                                                    "<div class='card-body'>" +
                                                        " <h5 class='card-title text-center'><b>" + inmuebles[n].InmuebleName + "</b></h5>     " +   
                                                        "<p class='card-text text-center alert alert-success'>" + inmuebles[n].Price + " €</p>" +
                                                        "<div class='text-center'>" +
                                                            "<a href='/Public/InmuebleView.aspx?id=" + inmuebles[n].InmuebleId + "' class='btn btn-success'>Visualizar</a>" +
                                                        "</div>" +
                                                    "</div>" +
                                                "</div>"
                            ;
                        container.appendChild(inmueble);
                        document.querySelector("#ItemPreview_" + inmuebles[n].InmuebleId).src = 'data:image/jpg;base64,' + inmuebles[n].Image;
                        n++;
                    });  
	            }
            });
         } );
    </script>
</asp:Content>
