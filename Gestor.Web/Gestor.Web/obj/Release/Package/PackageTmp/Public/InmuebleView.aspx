﻿<%@ Page Title="Detalle Inmueble" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="InmuebleView.aspx.cs" Inherits="Gestor.Web.Public.InmuebleView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h2 class='card-text text-center alert alert-success'><%: Title %></h2>

    <div style="padding-top: 50px">
        <div class="col-md-3">
            <asp:Image ID="imgInmueble" runat="server" Height="180px" Width="260px" class='card-img-top Image_<%: id %>' src="" />
        </div>
        <div class="col-md-9">
            <div class="form-group">
                <asp:Label ID="LabelNombre" runat="server" Text="Nombre" CssClass="col-md-3" AssociatedControlID="TextBoxNombre"></asp:Label>
                <asp:Label ID="TextBoxNombre" Text="" CssClass="col-md9" runat="server" />
            </div>
            <div class="form-group">
                <asp:Label ID="LabelPrecio" runat="server" Text="Precio" CssClass="col-md-3" AssociatedControlID="TextBoxPrecio"></asp:Label>
                <asp:Label ID="TextBoxPrecio" runat="server" CssClass="col-md9"></asp:Label>
            </div>
            <div class="form-group">
                <asp:Label ID="LabelMetros" runat="server" Text="Metros cuadrados" CssClass="col-md-3" AssociatedControlID="TextBoxMetros"></asp:Label>
                <asp:Label ID="TextBoxMetros" runat="server" CssClass="col-md9"></asp:Label>
            </div>
            <div class="form-group">
                <asp:Label ID="Label4" runat="server" Text="Calle" CssClass="col-md-3" AssociatedControlID="txtCalle"></asp:Label>
                <asp:Label ID="txtCalle" runat="server" CssClass="col-md9"></asp:Label>
            </div>
            <div class="form-group">
                <asp:Label ID="Label5" runat="server" Text="Nº" CssClass="col-md-3" AssociatedControlID="txtCalle2"></asp:Label>
                <asp:Label ID="txtCalle2" runat="server" CssClass="col-md9"></asp:Label>
            </div>
            <div class="form-group">
                <asp:Label ID="Label6" runat="server" Text="Ciudad" CssClass="col-md-3" AssociatedControlID="txtCiudad"></asp:Label>
                <asp:Label ID="txtCiudad" runat="server" CssClass="col-md9"></asp:Label>
            </div>
            <div class="form-group">
                <asp:Label ID="Label7" runat="server" Text="Provincia" CssClass="col-md-3" AssociatedControlID="txtProvincia"></asp:Label>
                <asp:Label ID="txtProvincia" runat="server" CssClass="col-md9"></asp:Label>
            </div>
            <div class="form-group">
                <asp:Label ID="Label1" runat="server" Text="Piscina" CssClass="col-md-3" AssociatedControlID="checkPiscina"></asp:Label>
                <asp:Label ID="checkPiscina" runat="server" CssClass="col-md9"/>
            </div>
            <div class="form-group">
                <asp:Label ID="Label2" runat="server" Text="Ascensor" CssClass="col-md-3" AssociatedControlID="checkAscensor"></asp:Label>
                <asp:Label ID="checkAscensor" CssClass="col-md9" runat="server" />
            </div>
            <div class="form-group">
                <asp:Label ID="Label3" runat="server" Text="Exterior" CssClass="col-md-3" AssociatedControlID="checkExterior"></asp:Label>
                <asp:Label ID="checkExterior" CssClass="col-md9" runat="server" />
            </div>
            <div class="form-group">
                <asp:Label ID="LabelDescripcion" runat="server" Text="Descripción" CssClass="col-md-3" AssociatedControlID="TextBoxDescripcion"></asp:Label>
                <asp:Label ID="TextBoxDescripcion" runat="server" class="col-md9" TextMode="SingleLine" Rows="2"></asp:Label>
            </div>

        </div>
    </div>

    <br />
    <br />
</asp:Content>
