﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gestor.CORE.Contracts
{
    public interface IDirectionManager : IGenericManager<Direction>
    {
        IQueryable<Direction> GetDirectionById(int id);
    }
}
