﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gestor.CORE.Contracts
{
    public interface IImageManager : IGenericManager<Photo>
    {
        IQueryable<Photo> GetImageByName(string name);
    }
}
