﻿using System.Linq;
using Gestor.CORE;

namespace Gestor.CORE.Contracts
{
    public interface IInmuebleManager : IGenericManager<Inmueble>
    {
        IQueryable<Inmueble> GetInmueblesPublicados();
        IQueryable<Inmueble> GetInmueblesSeguimiento();
        IQueryable<Inmueble> GetInmueblesCaptados();
        IQueryable<Inmueble> GetInmueblesCliente(string clienteId);

    }
}