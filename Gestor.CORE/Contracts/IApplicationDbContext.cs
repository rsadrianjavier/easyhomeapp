﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Gestor.CORE;

namespace Gestor.CORE.Contracts
{
    /// <summary>
    /// Interfaz del contexto
    /// </summary>
    public interface IApplicationDbContext
    {
        DbSet<Inmueble> Inmuebles { get; set; }
        DbSet<Direction> Direcciones { get; set; }
        DbSet<Photo> Images { get; set; }

        /// <summary>
        /// Guarda cambios en el contexto
        /// </summary>
        /// <returns>Número de elementos guardados</returns>
        int SaveChanges();

        DbEntityEntry Entry(object entity);

        /// <summary>
        /// Obtiene una entrada de una entidad del contexto
        /// </summary>
        /// <typeparam name="TEntity">Tipo de la entidad que queremos la entrada</typeparam>
        /// <param name="entity">Entidad de la que queremos su entrada</param>
        /// <returns>Entrada de la entidad</returns>
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;

        DbSet Set(Type entityType);

        /// <summary>
        /// Obtiene la colección de una entidad
        /// </summary>
        /// <typeparam name="TEntity">Entidad de la que queremos el contexto</typeparam>
        /// <returns>Colección de la entidad</returns>
        DbSet<TEntity> Set<TEntity>() where TEntity : class;

    }
}