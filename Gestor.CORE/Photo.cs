﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gestor.CORE
{
    /// <summary>
    /// Entidad de dominio de Imagenes
    /// </summary>
    public class Photo
    {
        /// <summary>
        /// Identificador de la imagen
        /// </summary>
        [Key]
        public int ImageId { get; set; }

        /// <summary>
        /// Nombre de la imagen
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Bytes de la imagen
        /// </summary>
        public string ImageBytes { get; set; }
    }
}
