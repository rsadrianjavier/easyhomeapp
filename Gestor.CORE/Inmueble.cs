﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gestor.CORE
{
    /// <summary>
    /// Entidad de dominio de inmuebles
    /// </summary>
    public class Inmueble
    {
        /// <summary>
        /// Identificador del inmueble
        /// </summary>
        [Key]
        public int InmuebleId { get; set; }

        /// <summary>
        /// Nombre del inmueble
        /// </summary>
        public string InmuebleName { get; set; }

        /// <summary>
        /// Precio del inmueble
        /// </summary>
        public double Price { get; set; }

        /// <summary>
        /// Indica la dirección del inmueble
        /// </summary>
        public Direction Direction { get; set; }

        /// <summary>
        /// Identificador de direccion del inmuble
        /// </summary>
        [ForeignKey("Direction")]
        public int DirectionId { get; set; }

        /// <summary>
        /// Indica si el inmueble está publicado
        /// </summary>
        public bool Publicado { get; set; }        

        /// <summary>
        /// Indica si el inmueble está captado
        /// </summary>
        public bool Captado { get; set; }

        /// <summary>
        /// Imagen del inmueble
        /// </summary>
        public Photo Image { get; set; }

        /// <summary>
        /// Identificador de la imagen del inmueble
        /// </summary>
        [ForeignKey("Image")]
        public int ImageId { get; set; }

        /// <summary>
        /// Propietario del inmueble
        /// </summary>
        public ApplicationUser Client { get; set; }

        /// <summary>
        /// Identificador del propietario del inmueble
        /// </summary>
        [ForeignKey("Client")]
        public string ClientId { get; set; }

        /// <summary>
        /// Agente que comienza el seguimiento del inmueble
        /// </summary>
        public ApplicationUser Agente { get; set; }

        /// <summary>
        /// Identificador agente que comienza el seguimiento del inmueble
        /// </summary>
        [ForeignKey("Agente")]
        public string AgentetId { get; set; }

        /// <summary>
        /// Metros cuadrados del inmueble
        /// </summary>
        public int metros { get; set; }

        /// <summary>
        /// Define si el inmueble es exterior o no
        /// </summary>
        public bool exterior { get; set; }

        /// <summary>
        /// Define la planta del inmueble
        /// </summary>
        public string Planta { get; set; }

        /// <summary>
        /// Define el numero de habitaciones del inmueble
        /// </summary>
        public int Habitaciones { get; set; }

        /// <summary>
        /// Define si el inmueble cuenta con piscina
        /// </summary>
        public bool Piscina { get; set; }

        /// <summary>
        /// Define si el inmueble cuenta con ascensor
        /// </summary>
        public bool Ascensor { get; set; }

        /// <summary>
        /// Descripcion del inmueble
        /// </summary>
        public string Description { get; set; }
    }
}
