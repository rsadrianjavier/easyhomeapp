﻿using Gestor.CORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gestor.DAL;
using Gestor.CORE.Contracts;

namespace Gestor.Application
{
    /// <summary>
    /// Clase manager de Inmueble
    /// </summary>
    public class InmuebleManager : GenericManager<Inmueble>, IInmuebleManager
    {
        /// <summary>
        /// Constructor de la clase Manager de Inmueble
        /// </summary>
        /// <param name="context">Contexto de datos</param>
        public InmuebleManager(IApplicationDbContext context) : base(context)
        {
        }

        /// <summary>
        /// Metodo que retorna todos los inmuebles publicados
        /// </summary>
        /// <returns>Todos los inmuebles publicados</returns>
        public IQueryable<Inmueble> GetInmueblesPublicados()
        {
            return Context.Inmuebles.Where(e => e.Publicado == true);
        }

        /// <summary>
        /// Metodo que retorna todos los inmuebles captados
        /// </summary>
        /// <param name="userId">Identificador de usuario</param>
        /// <returns>Todos los todos los inmuebles captados</returns>
        public IQueryable<Inmueble> GetInmueblesCaptados()
        {
            return Context.Inmuebles.Include("Direction").Where(e => e.Captado == true);
        }

        /// <summary>
        /// Metodo que retorna todos los inmuebles en seguimiento
        /// </summary>
        /// <returns>Todos los inmuebles en seguimiento</returns>
        public IQueryable<Inmueble> GetInmueblesSeguimiento()
        {
            return Context.Inmuebles.Include("Direction").Where(e => e.Captado == false);
        }

        /// <summary>
        /// Metodo que retorna todos los inmuebles de un cliente
        /// </summary>
        /// <param name="clienteId">Identificador del cliente</param>
        /// <returns>Todos los inmuebles de un cliente</returns>
        public IQueryable<Inmueble> GetInmueblesCliente(string clienteId)
        {
            return Context.Inmuebles.Include("Direction").Where(e => e.ClientId == clienteId);
        }

        /// <summary>
        /// Obtiene un inmueble por su clave int
        /// </summary>
        /// <param name="id">Identificador</param>
        /// <returns>Entidad si es encontrada</returns>
        public new Inmueble GetById(int id)
        {
            return Context.Inmuebles.Include("Direction").Where(e => e.InmuebleId == id).FirstOrDefault();
        }


    }
}
