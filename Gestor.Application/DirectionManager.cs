﻿using Gestor.CORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gestor.DAL;
using Gestor.CORE.Contracts;

namespace Gestor.Application
{
    /// <summary>
    /// Manager de Direction
    /// </summary>
    public class DirectionManager : GenericManager<Direction>, IDirectionManager
    {
        /// <summary>
        /// Constructor del manager de Image
        /// </summary>
        /// <param name="context">Contexto de datos</param>
        public DirectionManager(IApplicationDbContext context) : base(context)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public IQueryable<Direction> GetDirectionById(int idDirection)
        {
            return Context.Direcciones.Where(e => e.DirectionId == idDirection && e.DirectionId == idDirection);
        }
    }
}
