﻿using Gestor.CORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gestor.DAL;
using Gestor.CORE.Contracts;

namespace Gestor.Application
{
    /// <summary>
    /// Manager de Photo
    /// </summary>
    public class ImageManager : GenericManager<Photo>, IImageManager
    {
        /// <summary>
        /// Constructor del manager de Image
        /// </summary>
        /// <param name="context">Contexto de datos</param>
        public ImageManager(IApplicationDbContext context) : base(context)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public IQueryable<Photo> GetImageByName(string name)
        {
            return Context.Images.Where(e => e.Name == name);
        }
    }
}
