namespace Gestor.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tres : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Directions",
                c => new
                    {
                        DirectionId = c.Int(nullable: false, identity: true),
                        Province = c.String(),
                        Location = c.String(),
                        Street = c.String(),
                        Street2 = c.String(),
                    })
                .PrimaryKey(t => t.DirectionId);
            
            CreateTable(
                "dbo.Photos",
                c => new
                    {
                        ImageId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ImageBytes = c.String(),
                    })
                .PrimaryKey(t => t.ImageId);
            
            CreateTable(
                "dbo.Inmuebles",
                c => new
                    {
                        InmuebleId = c.Int(nullable: false, identity: true),
                        InmuebleName = c.String(),
                        Price = c.Double(nullable: false),
                        DirectionId = c.Int(nullable: false),
                        Publicado = c.Boolean(nullable: false),
                        Captado = c.Boolean(nullable: false),
                        ImageId = c.Int(nullable: false),
                        ClientId = c.String(maxLength: 128),
                        AgentetId = c.String(maxLength: 128),
                        metros = c.Int(nullable: false),
                        exterior = c.Boolean(nullable: false),
                        Planta = c.String(),
                        Habitaciones = c.Int(nullable: false),
                        Piscina = c.Boolean(nullable: false),
                        Ascensor = c.Boolean(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.InmuebleId)
                .ForeignKey("dbo.AspNetUsers", t => t.AgentetId)
                .ForeignKey("dbo.AspNetUsers", t => t.ClientId)
                .ForeignKey("dbo.Directions", t => t.DirectionId, cascadeDelete: true)
                .ForeignKey("dbo.Photos", t => t.ImageId, cascadeDelete: true)
                .Index(t => t.DirectionId)
                .Index(t => t.ImageId)
                .Index(t => t.ClientId)
                .Index(t => t.AgentetId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        documentoIdentidad = c.String(),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Inmuebles", "ImageId", "dbo.Photos");
            DropForeignKey("dbo.Inmuebles", "DirectionId", "dbo.Directions");
            DropForeignKey("dbo.Inmuebles", "ClientId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Inmuebles", "AgentetId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.Inmuebles", new[] { "AgentetId" });
            DropIndex("dbo.Inmuebles", new[] { "ClientId" });
            DropIndex("dbo.Inmuebles", new[] { "ImageId" });
            DropIndex("dbo.Inmuebles", new[] { "DirectionId" });
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Inmuebles");
            DropTable("dbo.Photos");
            DropTable("dbo.Directions");
        }
    }
}
