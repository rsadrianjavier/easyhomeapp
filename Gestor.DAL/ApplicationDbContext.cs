﻿using Gestor.CORE;
using Gestor.CORE.Contracts;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace Gestor.DAL
{
    //Enable-Migrations
    //Add-Migration "name xxxxx" -StartUpProjectName "Gestor.Web" -ConnectionStringName "DefaultConnection" -Verbose
    //Update-Database -StartUpProjectName "Gestor.Web" -ConnectionStringName "DefaultConnection" -Verbose 

    /// <summary>
    /// Clase de contexto de datos de Entity Framework
    /// </summary>
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IApplicationDbContext
    {

        /// <summary>
        /// Constructor por defecto de la clase
        /// </summary>
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        /// <summary>
        /// Metodo estatico para crear el contexto
        /// </summary>
        /// <returns>Contexto de datos</returns>
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        /// <summary>
        /// Coleccion persistible de inmuebles
        /// </summary>
        public DbSet<Inmueble> Inmuebles { get; set; }

        /// <summary>
        /// Coleccion persistible de direcciones
        /// </summary>
        public DbSet<Direction> Direcciones { get; set; }

        /// <summary>
        /// Colección persistible de imagenes
        /// </summary>
        public DbSet<Photo> Images { get; set; }
    }

}
